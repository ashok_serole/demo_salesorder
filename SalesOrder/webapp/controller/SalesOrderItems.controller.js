sap.ui.define([
	"sap/ui/core/mvc/Controller",
		"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"../model/formatter"
], function (Controller,JSONModel, Filter,formatter) {
	"use strict";

	return Controller.extend("demo.SalesOrder.SalesOrder.controller.SalesOrderItems", {
		formatter: formatter,
		
		onInit: function () {
				this.router = sap.ui.core.UIComponent.getRouterFor(this);
				this.router.attachRouteMatched(this._onObjectMatched,this);
			
		},
		_onObjectMatched:function(oEvent){
				let oTable = this.byId("idDetails"),filters = new Array();
			if(oEvent.getParameter("name") === 'salesOrderItems'){
				//	this.fnTableModel(oEvent);
					filters.push(new sap.ui.model.Filter('SalesOrderID',sap.ui.model.FilterOperator.EQ,oEvent.getParameter("arguments").salesOrderId));
					oTable.getBinding("items").filter(filters, sap.ui.model.FilterType.Application);
			}
		},
		/*fnTableModel:function(oEvent){
			let sModel = this.getOwnerComponent().getModel(),that = this, filters = new Array(),sPath='/SalesOrderLineItemSet';
			filters.push(new sap.ui.model.Filter('SalesOrderID',sap.ui.model.FilterOperator.EQ,oEvent.getParameter("arguments").salesOrderId));
			sModel.read(sPath,{
					context:null,
        			urlParameters:null,
        			async:false,
        			filters:filters,
        			success:function(oData,oResponse){
        				let tModel = new JSONModel({results:oData.results});
   						that.getView().setModel(tModel,'ItemModel');
        			},
        			error:function(oResponse){
            
        			 }
				});
			
		}*/
	

	
	});

});