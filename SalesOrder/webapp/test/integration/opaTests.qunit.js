/* global QUnit */

QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function() {
	"use strict";

	sap.ui.require([
		"demo/SalesOrder/SalesOrder/test/integration/AllJourneys"
	], function() {
		QUnit.start();
	});
});